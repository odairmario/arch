
function install() {
    # $1 package
    
    # verifica se está como root
    if [[ $EUID -ne 0 ]]; then
        sudo pacman -S $1 --noconfirm --quiet
    else
        pacman -S $1 --noconfirm --quiet
    fi
}

function load_value() {
    # $1 file_path
    # $2 key
    # $3 type
    a=`grep -i "$key" $1`
    
    if [ $? -eq 0 ]; then
        value="$(echo $a | cut -f 2 -d ':' | tr ' ' '\0')"
    else
        echo "conf file or patten not found."
        echo "put value of $2, value type is $3."
        read value
    fi

    return value
}

#!/usr/bin/env bash

source utils.sh

function locale_conf() {
    # descomenta idioma e codificação de /etc/locale.gen
    locale_path="/etc/locale.gen"
    sed -i "s/#pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/g" $locale_path
    sed -i "s/#pt_BR ISO-8859-1/pt_BR ISO-8859-1/g" $locale_path

    # coloca LANG=pt_BR.UTF-8 em /etc/locale.conf
    echo "LANG=pt_BR.UTF-8" > /etc/locale.conf

    locale-gen
    export LANG=pt_BR.UTF-8

    # configura layout do teclado
    echo "KEYMAP=br-abnt2" > /etc/vconsole.conf

    # cria link simbolico para fuso horario
    ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

    # configura o relógio
    echo "# UTC\nhwclock --systohc --utc" > /etc/adjtime
    echo "# LOCALTIME\nhwclock --systohc --localtime" >> /etc/adjtime

}

function host_dns() {
    # $1 file path 
    hostname=`load_value $1 "hostname" "string"`
    echo "hostname is: $hostname"
    echo "$hostname" > /etc/hostname

    # configura /etc/hosts
    echo "127.0.0.1	localhost" > /etc/hosts
    echo "::1		localhost" >> /etc/hosts
    echo "127.0.1.1	$hostname.localdomain	$hostname" >> /etc/hosts

    mkinitcpio -p linux

}

function network_conf() {
    # $1 file_path

    # configurar network manage
    install "networkmanager"

    # configurar wifi (se necessário)
    wifi=`load_value $1 "wifi" "boolean(0/1)"`
    if [[ $wifi -eq 1 ]]; then
        install "wireless_tools wpa_supplicant wifi-menu dialog"
    fi
}

function grub_conf_effi() {
    # $1 file_path
    # $2 test
    install "grub"
    install "efibootmgr"
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub

    # virtualbox
    if [[ $2 == 1 ]]; then
        #statements
        mv /boot/EFI/grub /boot/EFI/BOOT
        mv /boot/EFI/BOOT/grubx64.efi /boot/EFI/BOOT/bootx64.efi
    fi
    grub-mkconfig -o /boot/grub/grub.cfg


}

function system_conf() {
    # $1 conf_path
    locale_conf;
    host_dns $1;
    network_conf $1;
    grub_conf_effi $1 1;
    #user_conf;
}

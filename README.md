
## Arquivo: configure\_system
    * configurar localização
    * configurar host e dns
    * configurar rede
    * configurar o grub
    * configurar contas (usuario e senha root)

## Arquivo: configure\_video
    * instalar Xserver
    * instalar drive de video
    * configurar drive de video
    * configurar optimus/bumbleebe
    * configurar teclado

## Arquivo: configure\_programs
    * instalar programas necessários
    * configurar zsh

## Arquivo: configure\_vim
    * instlar vim e gvim
    * configurar vim e plugins

## Arquivo: configure\_graphical\_envioment
    * instalar interface grafica
    * configurar interface grafica



